let DOM = function () {
    return {
        input: $("#nameInput"),
        display: $("#display"),
        searchBtn: $("#search"),
        avg: $("#avg")
    };
}();
let appid = "e5cb46a61f3acf4374bcb6589565f515";
let searchedArr = [];
const getWeather = (name) => {
    $.ajax({
        method: "GET",
        url: "http://api.openweathermap.org/data/2.5/weather?q=" + name + "&appid=" + appid,
        success: (response) => {
            let res = {
                weather: response.weather[0].main,
                description: response.weather[0].description,
                icon: response.weather[0].icon,
                temp: response.main["temp"],
                temp_min: response.main["temp_min"],
                temp_max: response.main["temp_max"],
                wind: response.wind["speed"],
                name: response.name,
                sunrise: response.sys["sunrise"],
                sunset: response.sys["sunset"],
                country: response.sys["country"]
            };
            searchedArr.push(res.temp);
            draw(res);
        }, error: (error) => {
            console.log(error);
        }
    });
};
DOM.searchBtn.on('click', () => {
    makeAvg(searchedArr);
    getWeather(DOM.input[0]["value"]);
});
const draw = (city) => {
    let card = document.getElementsByName("template")[document.getElementsByName("template").length - 1].cloneNode(true);
    card.style.display = "inline-block";
    card.querySelector("#cityName").innerHTML = city.name;
    let img = card.querySelector("#img");
    img.setAttribute('src', `http://openweathermap.org/img/w/${city.icon}.png`);
    card.querySelector("#weather").innerHTML = city.weather + ", " + city.description;
    card.querySelector("#temp").innerHTML = "Temp: " + city.temp;
    card.querySelector("#minTemp").innerHTML = "min: " + city.temp_min;
    card.querySelector("#maxTemp").innerHTML = "max: " + city.temp_max;
    card.querySelector("#wind").innerHTML = "Wind: " + city.wind;
    card.querySelector("#sunrise").innerHTML = "Sunrise: " + city.sunrise;
    card.querySelector("#sunset").innerHTML = "Sunset: " + city.sunset;
    card.querySelector("#country").innerHTML = "Country: " + city.country;
    DOM.display.append(card);
};
const makeAvg = (arr) => {
    if (arr.length > 0) {
        let sum = arr.reduce((previous, current) => current += previous);
        let avg = sum / arr.length;
        DOM.avg.html("Average Temperature is: " + avg);
    }
};
