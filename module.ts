export class City {
    constructor(public weather: string, public description: string,public icon: string, public temp: number,
        public temp_min: number, public temp_max: number, public wind: any, public country: string,
        public sunrise: number, public sunset: number, public name: string){

        this.weather = weather;
        this.description = description;
        this.icon = icon;
        this.temp = temp;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.wind = wind;
        this.country = country;
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.name = name;


    }
}
